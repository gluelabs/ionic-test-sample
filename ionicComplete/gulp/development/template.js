var gulp = require('gulp'),
 templateCache = require('gulp-angular-templatecache');

var _config = require('../config');
var paths = _config.paths;

gulp.task('build:templates', function () {

  return gulp.src(paths.templates)
    .pipe(templateCache('app.templates.js', {
      module:'app.templates',
      standalone: true,
      root: 'templates/',//Indica la path da filtrare per cercare il file nella template cache
      templateHeader: "'use strict';\nangular.module('<%= module %>'<%= standalone %>).run(['$templateCache', function($templateCache) {",
      templateBody: "\n$templateCache.put('<%= url %>', '<%= contents %>');"
    }))
    .pipe(gulp.dest('./src'));
});
