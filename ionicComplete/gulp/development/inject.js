var gulp = require('gulp'),
  inject = require('gulp-inject'),
  rename = require('gulp-rename'),
  series = require('stream-series'),
  es = require('event-stream');

var _config = require('../config');
var paths = _config.paths;

var _transformAsync = function (filepath, file, i, length) {
  return '<script async src="' + filepath + '"></script>';
}

var _transformDefer = function (filepath, file, i, length) {
  return '<script defer src="' + filepath + '"></script>';
}

// Inject Stuff
//gulp.task('inject:dev', ['templates'], function () {
gulp.task('build:inject',['build:templates'], function () {
  var target = gulp.src('gulp/index-template.html');

  // Vendor JSs
  /*
  var vendorJsStream = gulp.src([
    'dist/css/vendor.min.css',
    'dist/css/theme.min.css',
    'dist/vendor.min.js'],
    { read: false }
  );
  */

  // App JSs
  // var modulesStream = gulp.src(PATHS.JS.SOURCE.MODULES, { read: false });
  //var sourceStream = gulp.src(PATHS.JS.SOURCE.COMPONENTS, { read: false });

  // Styles
  //var appStyleStream = gulp.src(PATHS.STYLE.CSS.APP, { read: false });
  console.log(paths.js);
  //Test Stream
  var appStream = gulp.src(paths.appJs, {
    read: false
  });

  var testStream = gulp.src(paths.js, {
    read: false
  });
  var templateStream = gulp.src(paths.renderedTemplates, {
    read: false
  });

  return target
    .pipe(inject(appStream, {
      name: 'inject:app',
      addPrefix: '/dev'
    }))
    .pipe(inject(testStream, {
      name: 'inject:test',
      addPrefix: '/dev'
    }))
    .pipe(inject(templateStream, {
      name: 'inject:template',
      addPrefix: '/dev'
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest('./www'));
  /*
   * Commented since it slow down too much startup time
   *
  .pipe(inject(
    es.merge(
      series(vendorStyleStream, themeStyleStream),
      series(vendorJsStream, themeJsStream)
    ), {
    name: 'inject:vendor',
    addPrefix: '/admin'
  }))
  */
  /*
  .pipe(inject(vendorJsStream, {
    name: 'inject:vendor',
    addPrefix: '/admin'
  }))
  .pipe(inject(modulesStream, {
    name: 'inject:source:maxpriority',
    addPrefix: '/admin'
  }))
  .pipe(inject(series(appStyleStream, sourceStream), {
    name: 'inject:source',
    addPrefix: '/admin'
  }))*/

});




gulp.task('move:js:dev', function () {

  gulp.src(paths.appJs).
  pipe(gulp.dest('./www/dev/src/common'));

  gulp.src(paths.js).
  pipe(gulp.dest('./www/dev/src'));

  gulp.src(paths.renderedTemplates).
  pipe(gulp.dest('./www/dev/src'));



});
