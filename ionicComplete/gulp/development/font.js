var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var jshint = require('gulp-jshint');
var javascriptObfuscator = require('gulp-javascript-obfuscator');

var _config = require('../config');
var paths = _config.paths;

gulp.task('build:fonts', function (done) {
  gulp.src(paths.fonts)
    .pipe(gulp.dest(paths.fontsTarget))
    .on('end', done);
});
