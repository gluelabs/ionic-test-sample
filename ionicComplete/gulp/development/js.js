var gulp = require('gulp');
var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var jshint = require('gulp-jshint');
var javascriptObfuscator = require('gulp-javascript-obfuscator');


var _config = require('../config');
var paths = _config.paths;

gulp.task('build:js', ['build:templates'], function (done) {
  gulp.src(paths.js)
    .pipe(concat('dist.js'))
    .pipe(gulp.dest('./www/js'))
    //.pipe(sourcemaps.init())
    .pipe(ngAnnotate())
    .pipe(uglify({ mangle: false }))
    .pipe(rename('dist.min.js'))
    //.pipe(sourcemaps.write('./'))
    //.pipe(javascriptObfuscator())
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});
gulp.task('jsOLD', function (done) {
  gulp.src(paths.js)
    .pipe(concat('dist.js'))
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});


gulp.task('build:js',['build:templates'], function (done) {
  gulp.src(paths.js)
    .pipe(concat('dist.js'))
    .pipe(gulp.dest('./www/js'))
    //.pipe(sourcemaps.init())
    .pipe(ngAnnotate())
    .pipe(uglify({ mangle: false }))
    .pipe(rename('dist.min.js'))
    //.pipe(sourcemaps.write('./'))
    //.pipe(javascriptObfuscator())
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});
gulp.task('js',['build:templates'], function (done) {
  gulp.src(paths.js)
    .pipe(concat('dist.js'))
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});


gulp.task('jsOffuscato', function (done) {
  gulp.src(paths.js)
    .pipe(concat('dist.js'))
    .pipe(gulp.dest('./www/js'))
    //.pipe(sourcemaps.init())
    .pipe(ngAnnotate())
    //.pipe(uglify({ mangle: false }))
    .pipe(rename('dist.min.js'))
    //.pipe(sourcemaps.write('./'))
    .pipe(javascriptObfuscator({
      compact: true,
      controlFlowFlattening: true,
      controlFlowFlatteningThreshold: 1,
      debugProtection: true,
      debugProtectionInterval: true,
      disableConsoleOutput: true,
      rotateStringArray: true,
      selfDefending: true,
      stringArray: true,
      stringArrayEncoding: 'rc4',
      stringArrayThreshold: 1,
      unicodeEscapeSequence: false
    }))
    .pipe(gulp.dest('./www/js'))
    .on('end', done);
});
