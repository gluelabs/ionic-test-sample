var gulp = require('gulp');
var path = require("path");
var karma = require('karma').Server;

gulp.task('test', function (done) {
  new karma({
    configFile: path.join(__dirname, '..', 'karma.conf.js'),
    singleRun: true
  }, done).start();
});

gulp.task('tdd', function (done) {
  new karma({
    configFile: path.join(__dirname, '..', 'karma.conf.js'),
    singleRun: false
  }, done).start();
});