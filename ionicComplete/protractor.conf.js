exports.config = {
  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      args: ['--disable-web-security']
    }
  },
  /*   
  multiCapabilities: [{
      'browserName': 'chrome'
    },
    {
      'browserName': 'chrome',
      'chromeOptions': {
        args: ['--disable-web-security']
      }
    }
  ],
 */

  baseUrl: 'http://localhost:8100',

  specs: [
    './e2e/**/*.test.js'
  ],

  framework: 'jasmine',

  jasmineNodeOpts: {
    isVerbose: true,
  }
};
