var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var jshint = require('gulp-jshint');
var javascriptObfuscator = require('gulp-javascript-obfuscator');

/**
 * Ricorsivamente vado a leggere nella cartella gulp
 * ed importo gli altri tasks
 */
require('require-dir')('./gulp', { recurse: true });

/**
 * Inizializzo localmente la cinfigurazione
 * contenuta nel config-json
 */
var _config = require('./gulp/config');
var paths = _config.paths;


gulp.task('default', ['watch']);
//gulp.task('serve:before', ['default']);
//gulp.task('build:before', ['default']);

gulp.task('sass', function (done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});


/**
 * Definizione dei task Wrapper
 */
gulp.task('build', ['build:sass','build:js','build:vendor','build:inject']);

/**
 * TODO: Configurare la DIST secondo esigenza
 */
gulp.task('dist', ['']);

gulp.task('build', ['build:fonts', 'build:vendor', 'build:sass', 'move:js:dev','build:inject']);



/**
 * Task Watch. IMPORTANTE perchè usato da ionic serve
 */

gulp.task('watch', ['build'], function () {
  gulp.watch(paths.sass, ['build:sass']);
  gulp.watch(paths.js, ['move:js:dev','build:inject']);
  gulp.watch(paths.templates, ['move:js:dev','build:inject']);
});


/*
 * 
 * Ulteriori Esempi Vari 
 * 
**/
gulp.task('jshint', function () {
  return gulp.src(paths.js)
  .pipe(jshint())
  .pipe(jshint.reporter());
});

/*
gulp.task('build:vendor', function() {
  return gulp.src(_config.vendor)
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.js'))
    .pipe(uglify({ mangle: true }))
    .pipe(rename('vendor.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('/www/js'));
    .pipe(jshint())
    .pipe(jshint.reporter());
});

gulp.task('install', ['git-check'], function () {
  return bower.commands.install()
    .on('log', function (data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function (done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
*/
