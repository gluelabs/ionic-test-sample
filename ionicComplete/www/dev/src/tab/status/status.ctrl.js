angular.module('starter.controllers')
  .controller('DashCtrl', function ($scope) {
    $scope.test = false;
    $scope.padre = {
      nome1Way: "Antonio 1",
      nome2Way: "Antonio 2"

    };

    $scope.receiver = function (m) {
      console.log('Sono il receiver del padre e mio figlio mi ha scritto: ' + m);
    }


    var success = function (message) {
      console.log("E' arrivata una risposta dal Pappagallo: " + message);
      alert('Il Pappagallo dice: ' + message);
    }

    var failure = function () {
      console.log("NON E' arrivata alcuna risposta dal Pappagallo: ");
      alert("Il Pappagallo non risponde");
    }

    //document.addEventListener("deviceready", onDeviceReady, false);

    /**
     * Runtime ?? Oppure on load??
     * Mosta esempio
     */
    var parrot = window.glueParrot;



    $scope.callParrot = function () {
      parrot = window.glueParrot;
      console.log('Sono il controller, chiamo il pappagallo!');

      if (parrot) {
        console.log("Provo a chiamare il pappagallo");
        var r = parrot.saluta("Antonio", success, failure);
      } else {
        alert("Il Pappagallo è morto")
      }
    }

    $scope.gimmeTime = function () {
      parrot = window.glueParrot;
      console.log('Sono il controller, chiamo il pappagallo!');

      if (parrot) {
        var r = parrot.gimmeTime(success, failure);
      } else {
        alert("Il Pappagallo è morto")
      }
    }

    $scope.faiDispetto = function () {
      parrot = window.glueParrot;
      console.log('Sono il controller, chiamo il pappagallo!');

      if (parrot) {
        var r = parrot.faiDispetto(success, failure);
      } else {
        alert("Il Pappagallo è morto")
      }
    }


  });
