angular.module('starter')
  .component('patrimonioDonut', {
    templateUrl: 'templates/tab/patrimonio/patrimonio-donut.cpt.html',
    controller: patrimonioDonutCtrl,
    bindings: {
      active : '<',
      data: '<',
      onRotate: '&'
    }
  });

function patrimonioDonutCtrl() {
  var ctrl = this;  


  ctrl.rotate = function(i){
    console.log("Sono nel Donut e voglio cambiare livello andando su "+i);
    ctrl.onRotate({"i":i});
  }
  

}

