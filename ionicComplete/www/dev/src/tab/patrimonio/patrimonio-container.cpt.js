angular.module('starter')
  .component('patrimonioContainer', {
    templateUrl: 'templates/tab/patrimonio/patrimonio-container.cpt.html',
    controller: patrimonioContainerCtrl,
    bindings: {
      accounts: "<"
    }
  });


function patrimonioContainerCtrl($scope) {
  var ctrl = this;
  
  /**
   * Usiamo un oggetto invece che una primitiva per evitare issue nel 1way data bindings
   * Le primitive vengono aggiornate "solo la prima volta"
   * #https://toddmotto.com/one-way-data-binding-in-angular-1-5/
   */
  ctrl.activeIndexObject = {
    "active": 0
  };

  /**
   * Questo metodo sarà usato dai Child per cambiare l'indice del conto corrente utilizzato un pò ovunque
   */
  ctrl.setIndex = function (i) {
    console.log("Sono il Padre, ho ricevuto una richiesta per cambiare l'indice da " + ctrl.activeIndexObject.active + " a " + i);
    ctrl.activeIndexObject = {
      "active": i
    };
  }











}
