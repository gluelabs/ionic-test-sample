angular.module('starter')
  .component('helloComponent', {
    templateUrl: 'components/_cpt/hello/hello.html',
    controller: HelloCtrl,
    bindings: {
      nomeStringa : '@',
      nome1Way: '<',
      nome2Way: '=',
      onConfirm: '&'
    }
  });









function HelloCtrl() {
  var ctrl = this;

  console.log('Il nome Stringa è ' + ctrl.nomeStringa);
  console.log('Il nome 1 Way è ' + ctrl.nome1Way);
  console.log('Il nome 2 Way è ' + ctrl.nome2Way);


  /**  
   * 
   * Differenza tra 1 way e 2 way DB
    ctrl.nome1Way = "Sovrascrivo";
    ctrl.nome2Way = "Sovrascrivo";
  
    console.log('Il nome Stringa è ' + ctrl.nomeStringa);
    console.log('Il nome 1 Way è ' + ctrl.nome1Way);
    console.log('Il nome 2 Way è ' + ctrl.nome1Way);
   */

  //console.log(ctrl.communicator)
  ctrl.confirm = function () {
    console.log('Sono il figlio, confermo');
    var r = "Ok, ho ricevuto";
    ctrl.onConfirm({m : r});
    
  }
}

