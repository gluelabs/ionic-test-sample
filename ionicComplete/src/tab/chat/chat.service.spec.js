describe('Chat Service', function() {

    var ChatsService, $httpBackend;

    // TODO: Load the Module to test
    beforeEach(module('starter.services'));  

    // TODO: Instantiate the Service and $httpBackend
    beforeEach(inject(function ($injector) {
        ChatsService = $injector.get('Chats');
        $httpBackend = $injector.get('$httpBackend');
    }));

    describe('#getRandomJoke', function() {

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should handle success after fetching a random joke', function() {

            $httpBackend
                .whenGET('https://api.icndb.com/jokes/random')
                .respond(42);

            ChatsService
                .getRandomJoke()
                .then(function(response) {
                    expect(response).toBe(42);
                });

            $httpBackend.flush();
        })

        it('should handle a 500 error while fetching a random joke', function() {

            $httpBackend
                .whenGET('https://api.icndb.com/jokes/random')
                .respond(500, 'Internal server error');
                
            ChatsService
                .getRandomJoke()
                .then(
                    angular.noop,
                    function (err) {
                        expect(ChatsService.getErrorCode()).toEqual(500);
                    }
                );

            $httpBackend.flush();
        })
    });
});