describe('ChatsCtrl', function () {

  var controller,
    ChatServiceMock,
    $scope;

  var chatExample = [{
    2: 'Ciao'
  }];
  var removeChat = 0;

  // Load the Module to test
  beforeEach(module('starter.controllers'));

  // Instantiate the Controller and Mocks
  beforeEach(inject(function ($rootScope, $controller, $q) {
    $scope = $rootScope.$new();

    ChatServiceMock = {
      all: jasmine.createSpy('all')
        .and.returnValue($q.resolve(chatExample)),
      remove: jasmine.createSpy('chat')
        .and.returnValue($q.resolve(removeChat))
    };

    controller = $controller('ChatsCtrl', {
      $scope: $scope,
      'Chats': ChatServiceMock
    });
  }));

  describe('Check the get of all the chats', function () {

    it('should call to method .all on the ChatService has been called', function () {
      expect(ChatServiceMock.all).toHaveBeenCalled();
    });
      it('Chats Value should not be undefined', function () {
      expect($scope.chats).not.toBe(undefined);
    });

  });

    describe('Check remove call is not fired', function () {

      it('Should not call remove method ot the ChatService', function () {
        expect(ChatServiceMock.remove).not.toHaveBeenCalled();
      });
      it('Chats Value should not be undefined', function () {
        expect($scope.chats).not.toBe(undefined);
      });


    });

});
