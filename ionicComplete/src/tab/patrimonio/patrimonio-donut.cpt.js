angular.module('starter')
  .component('patrimonioDonut', {
    templateUrl: 'templates/tab/patrimonio/patrimonio-donut.cpt.html',
    controller: patrimonioDonutCtrl,
    bindings: {
      active : '<',
      data: '<',
      onRotate: '&'
    }
  });

function patrimonioDonutCtrl() {
  var ctrl = this;  
  /**
   * Funzione esposta alla view per comunicare al padre
   * che è stato cambiato il conto corrente e richiedere un update
   */
  ctrl.rotate = function(i){
    console.log("Sono nel Donut e voglio cambiare in conto selezionato andando su "+i);
    ctrl.onRotate({"i":i});
  }
  

}

