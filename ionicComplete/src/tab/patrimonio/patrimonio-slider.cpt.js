angular.module('starter')
  .component('patrimonioSlider', {
    templateUrl: 'templates/tab/patrimonio/patrimonio-slider.cpt.html',
    controller: patrimonioSliderCtrl,
    bindings: {
      active: '<',
      data: '<',
      onSlide: '&'
    }
  });

function patrimonioSliderCtrl($scope) {
  var ctrl = this;
  ctrl.sliderDelegate;
  /**
   * Utiliziamo questo Lifecycle Hook per permettere allo slider
   * di ricevere aggiornamenti sul conto corrente selezionato 
   * dall'esterno
   */
  this.$onChanges = function (changes) {
    if (changes.active) {
      gotoSlide(changes.active.currentValue.active);
    }
  };
  /**
   * Funzione esposta alla view per comunicare al padre
   * che qualcosa è cambiato
   */
  ctrl.change = function (i) {
    console.log("Sono nello Slider e voglio cambiare conto andando su " + i);
    ctrl.onSlide({
      "i": i
    });
  }
  /**
   * Wrapper
   */
  function gotoSlide(i) {
    ctrl.sliderDelegate.slideTo(i);
  }
  /**
   * Inizializzazione del Delegate
   */
  $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
    ctrl.sliderDelegate = data.slider;
    gotoSlide(ctrl.active.active);
  });
  /**
   * Nel caso in cui vogliate triggerare l'evento all'inizio dello 
   * slide lo fate da qui
   */
  $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {

  });

  /**
   * Triggera la fine dello Swipe,quindi il momento giusto per notificare
   * il cambio index alle altre componenti
   */
  $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
    $scope.activeIndex = data.slider.activeIndex;
    //$scope.previousIndex = data.slider.previousIndex;
    ctrl.change(data.slider.activeIndex);
    $scope.$apply();
  });





}
