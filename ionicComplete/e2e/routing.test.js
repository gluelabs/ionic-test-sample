describe('Browsing the App', function () {



  browser.get('/#/tab/dash');
  browser.driver.sleep(2000);


  navTabs = [];
  element.all(by.css('.tab-nav.tabs a'))x
    .each(function (element, index) {
      navTabs.push(element);
    });




  beforeEach(function () {

  });

  it('should change the view pressing Chats Button', function () {
    navTabs[1].click();
    browser.driver.sleep(1500);
    browser.waitForAngular();
    browser.driver.sleep(1500);
    expect(browser.getCurrentUrl()).toMatch(/\/#\/tab\/chats$/);
  })

  
  it('should change the view pressing Account Button', function () {
    navTabs[2].click();
    browser.driver.sleep(1500);
    browser.waitForAngular();
    browser.driver.sleep(1500);
    expect(browser.getCurrentUrl()).toMatch(/\/#\/tab\/account$/);

  });

  it('should change the view pressing Status Button', function () {
    navTabs[0].click();
    browser.driver.sleep(1500);
    browser.waitForAngular();
    browser.driver.sleep(1500);
    expect(browser.getCurrentUrl()).toMatch(/\/#\/tab\/dash$/);

  });

  

});
