'use strict';

angular
    .module('starter.playlist')
    .factory('Playlist', function($timeout, $q, $http) {
        var _playlist = [
            { title: 'Reggae', id: 1 },
            { title: 'Chill', id: 2 },
            { title: 'Dubstep', id: 3 },
            { title: 'Indie', id: 4 },
            { title: 'Rap', id: 5 },
            { title: 'Cowbell', id: 6 }
        ];
        
        return {
            getAll: getAll,
            getOne: getOne,
            getRandomJoke: getRandomJoke
        };

        function getAll() {
            return $q.resolve(_playlist);
        }

        function getOne(id) {
            return $q.resolve(_playlist[id-1]);
        }

        function getRandomJoke() {
            var deferred = $q.defer();
            
            $http.get('https://api.icndb.com/jokes/random')
                .then(function(response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }
    });