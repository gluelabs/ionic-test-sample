angular
  .module('starter.playlist')
  .controller('PlaylistItemCtrl', function($scope, Playlist, $stateParams) {

    Playlist.getOne($stateParams.playlistId)
      .then(function(playlist) {
        $scope.playlist = playlist;
      });
  });