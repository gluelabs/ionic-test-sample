angular
  .module('starter.playlist', [])
  .controller('PlaylistCtrl', function($scope, Playlist) {

    $scope.dummyValue = 42;

    $scope.fetchPlaylist = function() {
      Playlist.getAll()
        .then(function(playlist) {
          $scope.playlists = playlist;
        });
    };

    $scope.fetchPlaylist();

  });