exports.config = {  
    
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {                
            args: ['--disable-web-security']
        } 
    },

    baseUrl: 'http://localhost:8100',

    specs: [
        'tests/e2e-tests/**/*.test.js'
    ],

    framework: 'jasmine',
    
    jasmineNodeOpts: {
        isVerbose: true,
    }
};